"""
The default configuration file. The values here should be overridden in the
actual configuration file used for the production and staging systems.
"""

import os

LOG_LEVEL = os.getenv("LOG_LEVEL", "WARNING")
SECRET_KEY = b"<Please! Please! Please! Change This!>"
UPLOAD_FOLDER = "/tmp/qc_app_files"
REDIS_URL = "redis://"
JOBS_TTL_SECONDS = 1209600 # 14 days
GNQC_REDIS_PREFIX="GNQC"
GN3_URL="http://localhost:8080"
SQL_URI = os.getenv(
    "SQL_URI", "mysql://gn2:mysql_password@localhost/db_webqtl_s")
