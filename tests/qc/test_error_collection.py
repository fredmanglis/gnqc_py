"""Check that error collection works as expected"""

import pytest

from functional_tools import take
from quality_control.parsing import FileType, collect_errors
from quality_control.errors import (
    InvalidValue, DuplicateHeading, InconsistentColumns)

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "sample,num,expected",
    ((range(0,25), 5, [0, 1, 2, 3, 4]),
     ([0, 1, 2, 3], 200, [0, 1, 2, 3]),
     (("he", "is", "a", "lovely", "boy"), 3, ["he", "is", "a"])))
def test_take(sample, num, expected):
    """Check that `take` works correctly."""
    taken = take(sample, num)
    assert len(taken) <= num
    assert taken == expected

@pytest.mark.slow
@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,filetype,count",
    (("tests/test_data/average_crlf.tsv", FileType.AVERAGE, 10),
     ("tests/test_data/average_error_at_end_200MB.tsv", FileType.AVERAGE,
      20),
     ("tests/test_data/average.tsv", FileType.AVERAGE, 5),
     ("tests/test_data/standarderror_1_error_at_end.tsv",
      FileType.STANDARD_ERROR, 13),
     ("tests/test_data/standarderror.tsv", FileType.STANDARD_ERROR, 9),
     ("tests/test_data/duplicated_headers_no_data_errors.tsv",
      FileType.AVERAGE, 10)))
def test_collect_errors(filepath, filetype, strains, count):
    """Check that `collect_errors` works as expected."""
    results = take(collect_errors(filepath, filetype, strains), count)
    def __valid_instance(item):
        return isinstance(item, (InvalidValue, DuplicateHeading))
    assert all(__valid_instance(error) for error in results)

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,filetype,expected",
    (("tests/test_data/average_inconsistent_columns.tsv", FileType.AVERAGE,
      (InconsistentColumns(
           4, 4, 5, "Header row has 4 columns while row 4 has 5 columns"),
       InconsistentColumns(
           5, 4, 3, "Header row has 4 columns while row 5 has 3 columns"),
       InconsistentColumns(
           6, 4, 7, "Header row has 4 columns while row 6 has 7 columns"))),))
def test_collect_inconsistent_column_errors(filepath, filetype, strains, expected):
    """
    Given: A file with inconsistent columns in certain lines
    When: collect_errors is run on the file
    Then: All the lines with inconsistent columns are flagged
    """
    assert tuple(collect_errors(filepath, filetype, strains)) == expected
