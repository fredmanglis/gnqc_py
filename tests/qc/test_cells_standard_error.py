"""Test standard error values."""
from random import randint


from hypothesis import given
from hypothesis import strategies as st

from quality_control.standard_error import invalid_value

@given(num_str=st.from_regex(r"^[0-9]+\.?0*", fullmatch=True))
def test_cell_standard_error_value_errors_if_less_than_six_decimal_places2(num_str):
    """
    GIVEN: `num_str` is a string representing a whole number (e.g 5, 33, 5453
        etc) or a number with all zeroes for the decimals (e.g 0.0, 19.00000,
        45842.00 etc)
    WHEN: `num_str` is provided as an argument to `se_invalid_value` function,
    THEN: `se_invalid_value` returns a `None`.
    """
    line, col = randint(0, 100), randint(0, 20)
    assert invalid_value(line, col, num_str) is None
