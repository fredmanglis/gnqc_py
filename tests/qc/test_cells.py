"""Test that values in cells within a line fulfill the required criteria"""

import re
from random import randint

import pytest
from hypothesis import given
from hypothesis import strategies as st

from quality_control.errors import InvalidValue
from quality_control.average import invalid_value as avg_invalid_value
from quality_control.standard_error import invalid_value as se_invalid_value

@pytest.mark.unit_test
@given(num_str=st.from_regex(
    r"^(?!(([0-9]+\.([0-9]{3}|[0-9]{6,}))|[0-9]+\.?0*)).*", fullmatch=True))
def test_cell_value_errors_with_invalid_inputs2(num_str):
    """
    GIVEN: `num_str` is an arbitrary string that is an invalid input,
    WHEN: `num_str` is provided as an argument to `*_invalid_value` functions,
    THEN: The `*_invalid_value` functions return a
      `quality_control.errors.InvalidValue` object which holds the error
      information.
    """
    assert avg_invalid_value(0, 0, num_str) == InvalidValue(
        0, 0, num_str, (
            f"Invalid value '{num_str}'. Expected string representing a number "
            "with exactly three decimal places."))
    assert se_invalid_value(0, 0, num_str) == InvalidValue(
        0, 0, num_str, (
            f"Invalid value '{num_str}'. Expected string representing a number "
            "with at least six decimal places."))

@given(num_str=st.from_regex(
    r"^[0-9]+\.([0-9]{1,2}|[0-9]{4,}$)", fullmatch=True).filter(
        lambda param: not re.match(r"^[0-9]+\.0+$", param)))
def test_cell_average_value_errors_if_not_three_decimal_places2(num_str):
    """
    GIVEN: `num_str` is a string representing a number with less than or more
      than three decimal places, e.g. 2.92, 39.483732
    WHEN: `num_str` is provided as an argument to `avg_invalid_value` function,
    THEN: `avg_invalid_value` returns a `quality_control.errors.InvalidValue`
      object with the information about the placement of the invalid value.
    """
    line, col = randint(0, 100), randint(0, 20)
    assert avg_invalid_value(line, col, num_str) == InvalidValue(
        line, col, num_str, (
            f"Invalid value '{num_str}'. Expected string representing a number "
            "with exactly three decimal places."))

@given(num_str=st.from_regex(r"^[0-9]+\.[0-9]{3}$", fullmatch=True))
def test_cell_average_value_pass_if_three_decimal_places(num_str):
    """
    GIVEN: `num_str` is a string representing a number with exactly three
      decimal places, e.g. 2.924, 39.483
    WHEN: `num_str` is provided as an argument to `avg_invalid_value` function,
    THEN: `avg_invalid_value` returns `None`
    """
    line, col = randint(0, 100), randint(0, 20)
    assert avg_invalid_value(line, col, num_str) is None

@given(num_str=st.from_regex(r"^[0-9]+\.([0-9]{0,5}$)", fullmatch=True).filter(
    lambda param: not re.match(r"^[0-9]+\.?0*$", param)))
def test_cell_standard_error_value_errors_if_less_than_six_decimal_places2(num_str):
    """
    GIVEN: `num_str` is a string representing a number with less than six
      decimal places, e.g. 2.9, 39.4837
    WHEN: `num_str` is provided as an argument to `se_invalid_value` function,
    THEN: `se_invalid_value` returns a `quality_control.errors.InvalidValue`
      object with the information about the placement of the invalid value.
    """
    line, col = randint(0, 100), randint(0, 20)
    assert se_invalid_value(line, col, num_str) == InvalidValue(
        line, col, num_str, (
            f"Invalid value '{num_str}'. Expected string representing a number "
            "with at least six decimal places."))

@given(num_str=st.from_regex(r"^[0-9]+\.[0-9]{6,}$", fullmatch=True))
def test_cell_standard_error_value_pass_if_six_or_more_decimal_places(num_str):
    """
    GIVEN: `num_str` is a string representing a number with six or more
      decimal places, e.g. 2.938434, 39.4837343
    WHEN: `num_str` is provided as an argument to `se_invalid_value` function,
    THEN: `se_invalid_value` returns `None`
    """
    line, col = randint(0, 100), randint(0, 20)
    assert se_invalid_value(line, col, num_str) is None
