"""Test average values."""
from random import randint
from hypothesis import given
from hypothesis import strategies as st

from quality_control.average import invalid_value as avg_invalid_value

@given(num_str=st.from_regex(r"^[0-9]+$", fullmatch=True))
def test_cell_average_value_pass_if_no_decimal_places(num_str):
    """
    GIVEN: `num_str` is a string representing a number with no decimal places,
      e.g. 2, 39, 8420
    WHEN: `num_str` is provided as an argument to `avg_invalid_value` function,
    THEN: `avg_invalid_value` returns `None`
    """
    line, col = randint(0, 100), randint(0, 20)
    assert avg_invalid_value(line, col, num_str) is None
