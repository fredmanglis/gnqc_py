"""Test loading of cross information."""

from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/test_cross_info_01.zip",
      ({"id": "1", "cross_direction": 1},
       {"id": "2", "cross_direction": 1},
       {"id": "3", "cross_direction": 1},
       {"id": "71", "cross_direction": 0},
       {"id": "72", "cross_direction": 0},
       {"id": "146", "cross_direction": 1},
       {"id": "147", "cross_direction": 1},
       {"id": "148", "cross_direction": 1})),
     ("tests/r_qtl/test_files/test_cross_info_02.zip",
      ({"id": "1", "cross_direction": 1},
       {"id": "2", "cross_direction": 1},
       {"id": "3", "cross_direction": 1},
       {"id": "71", "cross_direction": 0},
       {"id": "72", "cross_direction": 0},
       {"id": "146", "cross_direction": 1},
       {"id": "147", "cross_direction": 1},
       {"id": "148", "cross_direction": 1}))))
def test_parse_cross_info(filepath, expected):
    """Test parsing of cross information."""
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        assert tuple(rqtl2.cross_information(
            zfile, rqtl2.control_data(zfile))) == expected
