"""Test loading of sex information."""

from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/test_sex_info_01.zip",
      ({"id": "1", "sex": "male"},
       {"id": "2", "sex": "male"},
       {"id": "3", "sex": "male"},
       {"id": "71", "sex": "male"},
       {"id": "72", "sex": "male"},
       {"id": "146", "sex": "female"},
       {"id": "147", "sex": "female"},
       {"id": "148", "sex": "female"})),
     ("tests/r_qtl/test_files/test_sex_info_02.zip",
      ({"id": "1", "sex": "male"},
       {"id": "2", "sex": "male"},
       {"id": "3", "sex": "male"},
       {"id": "71", "sex": "male"},
       {"id": "72", "sex": "male"},
       {"id": "146", "sex": "female"},
       {"id": "147", "sex": "female"},
       {"id": "148", "sex": "female"}))))
def test_parse_sex_info(filepath, expected):
    """Test parsing of sex information."""
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        assert tuple(rqtl2.sex_information(
            zfile, rqtl2.control_data(zfile))) == expected
