"""Test accessing nonexistent member."""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,memberkey",
    (("tests/r_qtl/test_files/nonexistent.zip", "geno"),
     ("tests/r_qtl/test_files/nonexistent.zip", "founder_geno"),
     ("tests/r_qtl/test_files/nonexistent.zip", "pheno"),
     ("tests/r_qtl/test_files/nonexistent.zip", "covar"),
     ("tests/r_qtl/test_files/nonexistent.zip", "phenocovar"),
     ("tests/r_qtl/test_files/nonexistent.zip", "gmap"),
     ("tests/r_qtl/test_files/nonexistent.zip", "pmap")))
def test_loading_nonexistent_file(filepath, memberkey):
    """
    GIVEN: A zipfile with a valid control file, but some files mentioned in the
        control file do not exist in the zipfile
    WHEN: access is made to such missing files
    THEN: raise an exception
    """
    with (ZipFile(Path(filepath).absolute(), "r") as zfile,
          pytest.raises(rqtl2.InvalidFormat)):
        cdata = rqtl2.control_data(zfile)
        tuple(rqtl2.file_data(zfile, memberkey, cdata))
