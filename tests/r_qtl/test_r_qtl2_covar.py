"""Test parsing of 'covar' files."""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/test_covar.zip",
      ({"id": "1", "sex": "male", "cross_direction": 1},
       {"id": "2", "sex": "male", "cross_direction": 1},
       {"id": "3", "sex": "male", "cross_direction": 1},
       {"id": "71", "sex": "male", "cross_direction": 0},
       {"id": "72", "sex": "male", "cross_direction": 0},
       {"id": "146", "sex": "female", "cross_direction": 1},
       {"id": "147", "sex": "female", "cross_direction": 1},
       {"id": "148", "sex": "female", "cross_direction": 1})),
     ("tests/r_qtl/test_files/test_covar_transposed.zip",
      ({"id": "1", "sex": "male", "cross_direction": 1},
       {"id": "2", "sex": "male", "cross_direction": 1},
       {"id": "3", "sex": "male", "cross_direction": 1},
       {"id": "146", "sex": "female", "cross_direction": 1},
       {"id": "147", "sex": "female", "cross_direction": 1},
       {"id": "148", "sex": "female", "cross_direction": 1})),
     ))
def test_parse_covar_files(filepath, expected):
    """Test parsing of 'covar' files from the R/qtl2 bundle.

    GIVEN: path to a R/qtl2 bundle
    WHEN: the 'covar' file is parsed
    THEN: verify the parsed data is as expected
    """
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        process_fns = rqtl2.make_process_data_covar(cdata)
        assert tuple(
            rqtl2.file_data(zfile, "covar", cdata, *process_fns)) == expected
