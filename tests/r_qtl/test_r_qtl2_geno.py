"""Test the parsing of the R/qtl2 geno files."""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "relpath,expected",
    (
        ("tests/r_qtl/test_files/test_geno.zip",
         ({
             "id": "1",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "2",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "3",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": None,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "4",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "5",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
           },
          {
             "id": "6",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
           },
          {
             "id": "7",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "8",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 1,
             "EC.66C": 1
           },
          {
             "id": "9",
             "PVV4": None,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
           },
          {
             "id": "10",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
           })),
        ("tests/r_qtl/test_files/test_geno_transposed.zip",
         ({
             "id": "1",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
         }, {
             "id": "2",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
         }, {
             "id": "3",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": None,
             "EC.480C": 1,
             "EC.66C": 1
         }, {
             "id": "4",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
         },{
             "id": "5",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
         }, {
             "id": "6",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
         }, {
             "id": "7",
             "PVV4": 1,
             "AXR-1": 1,
             "HH.335C-Col/PhyA": 1,
             "EC.480C": 1,
             "EC.66C": 1
         }, {
             "id": "8",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 1,
             "EC.66C": 1
         }, {
             "id": "9",
             "PVV4": None,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
         }, {
             "id": "10",
             "PVV4": 2,
             "AXR-1": 2,
             "HH.335C-Col/PhyA": 2,
             "EC.480C": 2,
             "EC.66C": 2
         }))))
def test_parse_geno_files(relpath, expected):
    """Test parsing of geno files from the R/qtl2 bundle.

    GIVEN: Path to a zip file with R/qtl2 data
    WHEN: we parse the geno file
    THEN: ensure that the data we get is as expected
    """
    with ZipFile(Path(relpath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        assert tuple(rqtl2.file_data(
            zfile,
            "geno",
            cdata,
            *rqtl2.make_process_data_geno(cdata))) == expected

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "relpath,expected",
    (("tests/r_qtl/test_files/test_founder_geno.zip",
      (({
          "id": "1",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "2",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "3",
          "PVV4": 2,
          "AXR-1": 2,
          "HH.335C-Col/PhyA": None,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "4",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "5",
          "PVV4": None,
          "AXR-1": 2,
          "HH.335C-Col/PhyA": 2,
          "EC.480C": 2,
          "EC.66C": 2
      }))),
     ("tests/r_qtl/test_files/test_founder_geno_transposed.zip",
      (({
          "id": "1",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "2",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "3",
          "PVV4": 2,
          "AXR-1": 2,
          "HH.335C-Col/PhyA": None,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "4",
          "PVV4": 1,
          "AXR-1": 1,
          "HH.335C-Col/PhyA": 1,
          "EC.480C": 1,
          "EC.66C": 1
      }, {
          "id": "5",
          "PVV4": None,
          "AXR-1": 2,
          "HH.335C-Col/PhyA": 2,
          "EC.480C": 2,
          "EC.66C": 2
      })))))
def test_parse_founder_geno_files(relpath, expected):
    """Test parsing of founder_geno files from the R/qtl2 bundle.

    GIVEN: Path to a zip file with R/qtl2 data
    WHEN: we parse the founder_geno file
    THEN: ensure that the data we get is as expected
    """
    with ZipFile(Path(relpath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        assert tuple(rqtl2.file_data(
            zfile,
            "founder_geno",
            cdata,
            *rqtl2.make_process_data_geno(cdata))) == expected
