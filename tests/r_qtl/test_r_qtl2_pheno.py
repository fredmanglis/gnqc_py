"""Test parsing of pheno files in a R/qtl2 bundle."""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/test_pheno.zip",
      ({"id": "1", "liver": "61.92", "spleen": "153.16"},
       {"id": "2", "liver": "88.33", "spleen": "178.58"},
       {"id": "3", "liver": "58", "spleen": "131.91"},
       {"id": "4", "liver": "78.06", "spleen": None},
       {"id": "5", "liver": None, "spleen": "181.05"})),
     ("tests/r_qtl/test_files/test_pheno_transposed.zip",
      ({"id": "1", "liver": "61.92", "spleen": "153.16"},
       {"id": "2", "liver": "88.33", "spleen": "178.58"},
       {"id": "3", "liver": "58", "spleen": "131.91"},
       {"id": "4", "liver": "78.06", "spleen": None},
       {"id": "5", "liver": None, "spleen": "181.05"}))))
def test_parse_pheno_files(filepath, expected):
    """Test parsing of 'pheno' files from the R/qtl2 bundle.

    GIVEN: path to a R/qtl2 bundle
    WHEN: the 'pheno' file is parsed
    THEN: verify the parsed data is as expected
    """
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        assert tuple(rqtl2.file_data(zfile, "pheno", cdata)) == expected

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/test_phenocovar.zip",
      ({"id": "T0", "time (hrs)": "0"},
       {"id": "T2", "time (hrs)": "0.0333333333333333"},
       {"id": "T4", "time (hrs)": "0.0666666666666667"},
       {"id": "T6", "time (hrs)": "0.1"},
       {"id": "T8", "time (hrs)": "0.133333333333333"})),
     ("tests/r_qtl/test_files/test_phenocovar_transposed.zip",
      ({"id": "T0", "time (hrs)": "0"},
       {"id": "T2", "time (hrs)": "0.0333333333333333"},
       {"id": "T4", "time (hrs)": "0.0666666666666667"},
       {"id": "T6", "time (hrs)": "0.1"},
       {"id": "T8", "time (hrs)": "0.133333333333333"}))))
def test_parse_phenocovar_files(filepath, expected):
    """Test parsing of 'phenocovar' files from the R/qtl2 bundle.

    GIVEN: path to a R/qtl2 bundle
    WHEN: the 'phenocovar' file is parsed
    THEN: verify the parsed data is as expected
    """
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        assert tuple(rqtl2.file_data(zfile, "phenocovar", cdata)) == expected
