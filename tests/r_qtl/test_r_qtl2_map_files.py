"""Test the parsing of the R/qtl2 gmap files."""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "relpath,mapfiletype,expected",
    (("tests/r_qtl/test_files/test_gmap.zip",
      "gmap",
      ({"id": "PVV4", "chr": "1", "pos": "0.000000"},
       {"id": "AXR-1", "chr": "1", "pos": "6.250674"},
       {"id": "HH.335C-Col/PhyA", "chr": "1", "pos": "9.303868"},
       {"id": "EC.480C", "chr": "1", "pos": "12.577629"},
       {"id": "EC.66C", "chr": "1", "pos": "18.392830"})),
     ("tests/r_qtl/test_files/test_gmap_transposed.zip",
      "gmap",
      ({"id": "PVV4", "chr": "1", "pos": "0.000000"},
       {"id": "AXR-1", "chr": "1", "pos": "6.250674"},
       {"id": "HH.335C-Col/PhyA", "chr": "1", "pos": "9.303868"},
       {"id": "EC.480C", "chr": "1", "pos": "12.577629"},
       {"id": "EC.66C", "chr": "1", "pos": "18.392830"})),
     ("tests/r_qtl/test_files/test_pmap.zip",
      "pmap",
      ({"id": "D1Mit18", "chr": "1", "pos": "52.418656"},
       {"id": "D1Mit80", "chr": "1", "pos": "86.377953"},
       {"id": "D1Mit17", "chr": "1", "pos": "189.571337"},
       {"id": "D2Mit379", "chr": "2", "pos": "37.451062"},
       {"id": "D2Mit75", "chr": "2", "pos": "80.584782"})),
     ("tests/r_qtl/test_files/test_pmap_transposed.zip",
      "pmap",
      ({"id": "D1Mit18", "chr": "1", "pos": "52.418656"},
       {"id": "D1Mit80", "chr": "1", "pos": "86.377953"},
       {"id": "D1Mit17", "chr": "1", "pos": "189.571337"},
       {"id": "D2Mit379", "chr": "2", "pos": "37.451062"},
       {"id": "D2Mit75", "chr": "2", "pos": "80.584782"}))))
def test_parse_map_files(relpath, mapfiletype, expected):
    """
    GIVEN: A path to a zip file, `relpath` and the type of the map file,
    WHEN: we parse the R/qtl2 map file,
    THEN: ensure the parsed data is as expected.
    """
    with ZipFile(Path(relpath).absolute(), "r") as zfile:
        cdata = rqtl2.control_data(zfile)
        assert tuple(rqtl2.file_data(zfile, mapfiletype, cdata)) == expected
