"""Test the parsing of control file"""
from pathlib import Path

import pytest
from zipfile import ZipFile

from r_qtl import r_qtl2 as rqtl2

__DEFAULTS__ = {
    "description": "Empty control file to test defaults",
    "sep": ",",
    "na.strings": ["NA"],
    "comment.char": "#",
    "geno_transposed": False,
    "founder_geno_transposed": False,
    "pheno_transposed": False,
    "covar_transposed": False,
    "phenocovar_transposed": False,
    "gmap_transposed": False,
    "pmap_transposed": False,
    "phenose_transposed": False
}

@pytest.mark.unit_test
@pytest.mark.parametrize(
    "filepath,expected",
    (("tests/r_qtl/test_files/empty_control_file_yaml.zip",
      __DEFAULTS__),
     ("tests/r_qtl/test_files/empty_control_file_json.zip",
      __DEFAULTS__)))
def test_defaults_are_set(filepath, expected):
    """
    GIVEN: path to a R/qtl2 bundle with an empty control file
    WHEN: the control data is parsed
    THEN: ensure all the defaults are set up correctly
    """
    with ZipFile(Path(filepath).absolute(), "r") as zfile:
        assert rqtl2.control_data(zfile) == expected
