"""
The tests configuration file.
"""

import os

LOG_LEVEL = os.getenv("LOG_LEVEL", "WARNING")
SECRET_KEY = b"<Please! Please! Please! Change This!>"
UPLOAD_FOLDER = "/tmp/qc_app_files"
REDIS_URL = "redis://"
JOBS_TTL_SECONDS = 600 # 10 minutes
