"Test that the progress indication works correctly"

def test_with_non_existing_job(client, redis_conn_with_fresh_job): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a fresh, unstarted job
    WHEN: The parsing progress page is loaded for a non existing job
    THEN: Ensure that the page:
          1. Has a meta tag to redirect it to the index page after 5 seconds
          2. Has text indicating that the job does not exist
    """
    job_id = "non-existent-job-id"
    resp = client.get(f"/parse/status/{job_id}")
    assert resp.status_code == 400
    assert (
        b"No job, with the id '<em>non-existent-job-id</em>' was found!"
        in resp.data)
    assert b'<meta http-equiv="refresh" content="5;url=/">' in resp.data

def test_with_unstarted_job(client, job_id, redis_conn_with_fresh_job): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a fresh, unstarted job
    WHEN: The parsing progress page is loaded
    THEN: Ensure that the page:
          1. Has a meta tag to refresh it after 5 seconds
          2. Has a progress indicator with zero progress
    """
    resp = client.get(f"/parse/status/{job_id}")
    assert b'<meta http-equiv="refresh" content="5">' in resp.data
    assert (
        b'<progress id="job_' +
        (f'{job_id}').encode("utf8") +
        b'" value="0.0">0.0</progress>') in resp.data

def test_with_in_progress_no_error_job(
        client, job_id, redis_conn_with_in_progress_job_no_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a job in progress, with no errors found in
              the file so far
    WHEN: The parsing progress page is loaded
    THEN: Ensure that the page:
          1. Has a meta tag to refresh it after 5 seconds
          2. Has a progress indicator with the percent of the file processed
             indicated
    """
    resp = client.get(f"/parse/status/{job_id}")
    assert b'<meta http-equiv="refresh" content="5">' in resp.data
    assert (
        b'<progress id="job_' +
        (f'{job_id}').encode("utf8") +
        b'" value="0.32242342">32.242342</progress>') in resp.data
    assert (
        b'<span >No errors found so far</span>'
        in resp.data)
    assert b"<table" not in resp.data

def test_with_in_progress_job_with_errors(
        client, job_id, redis_conn_with_in_progress_job_some_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a job in progress, with some errors found in
              the file so far
    WHEN: The parsing progress page is loaded
    THEN: Ensure that the page:
          1. Has a meta tag to refresh it after 5 seconds
          2. Has a progress indicator with the percent of the file processed
             indicated
          3. Has a table showing the errors found so far
    """
    resp = client.get(f"/parse/status/{job_id}")
    assert b'<meta http-equiv="refresh" content="5">' in resp.data
    assert (
        b'<progress id="job_' +
        (f'{job_id}').encode("utf8") +
        b'" value="0.4534245">45.34245</progress>') in resp.data
    assert (
        b'<p class="alert-error">We have found the following errors so far</p>'
        in resp.data)
    assert b'table class="reports-table">' in resp.data
    assert b'Duplicate Header' in resp.data
    assert b'Invalid Value' in resp.data

def test_with_completed_job_no_errors(
        client, job_id, redis_conn_with_completed_job_no_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a completed job, with no errors found in
              the file so far
    WHEN: The parsing progress page is loaded
    THEN: Ensure that the response is a redirection to the results page
    """
    resp = client.get(f"/parse/status/{job_id}")
    assert resp.status_code == 302
    assert f"/parse/results/{job_id}".encode("utf8") in resp.data

def test_with_completed_job_some_errors(
        client, job_id, redis_conn_with_completed_job_no_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a completed job, with some errors found in
              the file so far
    WHEN: The parsing progress page is loaded
    THEN: Ensure that the response is a redirection to the results page
    """
    resp = client.get(f"/parse/status/{job_id}")
    assert resp.status_code == 302
    assert f"/parse/results/{job_id}".encode("utf8") in resp.data
