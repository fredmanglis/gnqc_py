"""Test the entry module in the web-ui"""
import pytest

from tests.conftest import uploadable_file_object

def test_basic_elements_present_in_index_page(client):
    """
    GIVEN: A flask application testing client
    WHEN: the index page is requested with the "POST" method and no datat
    THEN: verify that the response contains error notifications
    """
    response = client.get("/")
    assert response.status_code == 200
    ## form present
    assert b'<form action="/"' in response.data
    assert b'method="POST"' in response.data
    assert b'enctype="multipart/form-data"' in response.data
    assert b'</form>' in response.data
    ## filetype elements
    assert b'<input type="radio" name="filetype"' in response.data
    assert b'id="filetype_standard_error"' in response.data
    assert b'id="filetype_average"' in response.data
    ## file upload elements
    assert b'<label for="file_upload"' in response.data
    assert b'select file' in response.data
    assert b'<input type="file" name="qc_text_file"' in response.data
    assert b'id="file_upload"' in response.data
    ## submit button
    assert b'<input type="submit" value="upload file"' in response.data

def test_post_notifies_errors_if_no_data_is_provided(client):
    """
    GIVEN: A flask application testing client
    WHEN: the index page is requested with the "POST" method and with no
          data provided
    THEN: ensure the system responds woit the appropriate error messages
    """
    response = client.post("/", data={})
    assert response.status_code == 400
    assert (
        b'<span class="alert alert-error">Invalid file type provided.</span>'
        in response.data)
    assert (
        b'<span class="alert alert-error">No file was uploaded.</span>'
        in response.data)

def test_post_with_correct_data(client):
    """
    GIVEN: A flask application testing client
    WHEN: the index page is requested with the "POST" method and with the
          appropriate data provided
    THEN: ensure the system redirects to the parse endpoint with the filename
          and filetype
    """
    response = client.post(
        "/", data={
            "speciesid": 1,
            "filetype": "average",
            "qc_text_file": uploadable_file_object("no_data_errors.tsv")
        })

    assert response.status_code == 302
    assert b'Redirecting...' in response.data
    assert (
        b'/parse/parse?speciesid=1&amp;filename=no_data_errors.tsv&amp;filetype=average'
        in response.data)

@pytest.mark.parametrize(
    "request_data,error_message",
    (({"filetype": "invalid_choice",
       "qc_text_file": uploadable_file_object("no_data_errors.tsv")},
      b'<span class="alert alert-error">Invalid file type provided.</span>'),
     ({"filetype": "average"},
      b'<span class="alert alert-error">No file was uploaded.</span>'),
     ({"filetype": "standard-error"},
      b'<span class="alert alert-error">No file was uploaded.</span>')))
def test_post_with_missing_or_invalid_data(client, request_data,error_message):
    """
    GIVEN: A flask application testing client
    WHEN: the index page is requested with the "POST" method and with data
          either being missing or invalid
    THEN: ensure that the system responds with the appropriate error message
    """
    response = client.post("/", data=request_data)
    assert response.status_code == 400
    assert error_message in response.data
