"""Test the upload of zip files"""
from tests.conftest import uploadable_file_object

def test_upload_zipfile_with_zero_files(client):
    """
    GIVEN: A flask application testing client
    WHEN: A zip file with no files is uploaded
    THEN: Ensure that the system responds with the appropriate error message and
          status code
    """
    resp = client.post("/", data={
        "filetype": "average",
        "qc_text_file": uploadable_file_object("empty.zip")})
    assert resp.status_code == 400
    assert (b"Expected exactly one (1) member file within the uploaded zip "
            b"file. Got 0 member files.") in resp.data

def test_upload_zipfile_with_multiple_files(client):
    """
    GIVEN: A flask application testing client
    WHEN: A zip file with more than one file is uploaded
    THEN: Ensure that the system responds with the appropriate error message and
          status code
    """
    resp = client.post("/", data={
        "filetype": "average",
        "qc_text_file": uploadable_file_object("multiple_files.zip")})
    assert resp.status_code == 400
    assert (b"Expected exactly one (1) member file within the uploaded zip "
            b"file. Got 3 member files.") in resp.data

def test_upload_zipfile_with_one_tsv_file(client):
    """
    GIVEN: A flask application testing client
    WHEN: A zip file with exactly one valid TSV file is uploaded
    THEN: Ensure that the system redirects to the correct next URL
    """
    resp = client.post("/", data={
        "speciesid": 1,
        "filetype": "average",
        "qc_text_file": uploadable_file_object("average.tsv.zip")})
    assert resp.status_code == 302
    assert b"Redirecting..." in resp.data
    assert (
        b"/parse/parse?speciesid=1&amp;filename=average.tsv.zip&amp;filetype=average"
        in resp.data)

def test_upload_zipfile_with_one_non_tsv_file(client):
    """
    GIVEN: A flask application testing client
    WHEN: A zip file with exactly one file, which is not a valid TSV, is
          uploaded
    THEN: Ensure that the system responds with the appropriate error message and
          status code
    """
    resp = client.post("/", data={
        "filetype": "average",
        "qc_text_file": uploadable_file_object("non_tsv.zip")})
    assert resp.status_code == 400
    assert (b"Expected the member text file in the uploaded zip file to "
            b"be a tab-separated file.") in resp.data
