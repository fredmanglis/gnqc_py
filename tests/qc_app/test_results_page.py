"Test results page"

def test_results_with_stderr_output(
        client, job_id, stderr_with_output, # pylint: disable=[unused-argument]
        redis_conn_with_in_progress_job_no_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A file with content to simulate the stderr output
           3. A sample job to prevent the "No such job" error message
    WHEN: The parsing progress page is loaded for a non existing job
    THEN: Ensure that the page:
          1. Redirects to a job failure display page
          2. The job failure display page:
             a) indicates that this is a worker failure
             b) provides some debugging information
    """
    # Maybe get rid of the use of a stderr file, and check for actual exceptions
    resp = client.get(f"/parse/status/{job_id}", follow_redirects=True)
    assert len(resp.history) == 1
    assert b'<h1 class="heading">Worker Failure</h1>' in resp.data
    assert b'<h4>Debugging Information</h4>' in resp.data
    assert (
        f"<li><strong>job id</strong>: {job_id}</li>".encode("utf8")
        in resp.data)

def test_results_with_completed_job_no_errors(
        client, job_id, stderr_with_no_output, # pylint: disable=[unused-argument]
        redis_conn_with_completed_job_no_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a completed job, with no errors found in
              the file
           3. A file with no contents to simulate no stderr output
    WHEN: The parsing progress page is loaded
    THEN: Ensure that:
          1. the system redirects to the results page
          2. the results page indicates that there are no errors in the file
             being processed
    """
    resp = client.get(f"/parse/status/{job_id}", follow_redirects=True)
    assert len(resp.history) == 1
    assert (
        b'<span class="alert-success">No errors found in the file</span>'
        in resp.data)

def test_results_with_completed_job_some_errors(
        client, job_id, stderr_with_no_output, # pylint: disable=[unused-argument]
        redis_conn_with_completed_job_some_errors): # pylint: disable=[unused-argument]
    """
    GIVEN: 1. A flask application testing client
           2. A redis instance with a completed job, with some errors found in
              the file
           3. A file with no contents to simulate no stderr output
    WHEN: The parsing progress page is loaded
    THEN: Ensure that:
          1. the system redirects to the results page
          2. the results page displays the errors found
    """
    resp = client.get(f"/parse/status/{job_id}", follow_redirects=True)
    assert len(resp.history) == 1
    assert (
        b'<p class="alert-error">We found the following errors</p>'
        in resp.data)
    assert b'<table class="reports-table">' in resp.data
    assert b'Duplicate Header' in resp.data
    assert b'<td>Heading &#39;DupHead&#39; is repeated</td>' in resp.data
    assert b'Invalid Value' in resp.data
    assert b'<td>Invalid value &#39;ohMy&#39;</td>' in resp.data
