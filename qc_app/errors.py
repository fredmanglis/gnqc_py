"""Application error handling."""
import logging
import traceback

import MySQLdb as mdb
from flask import Flask, request, render_template

def handle_general_exception(exc: Exception):
    """Handle generic exceptions."""
    trace = traceback.format_exc()
    logging.error(
        "Error (%s.%s): Generic unhandled exception!! (URI: %s)\n%s",
        exc.__class__.__module__, exc.__class__.__name__, request.url, trace)
    return render_template("unhandled_exception.html", trace=trace)

def register_error_handlers(app: Flask):
    """Register top-level error/exception handlers."""
    app.register_error_handler(Exception, handle_general_exception)
    app.register_error_handler(mdb.MySQLError, handle_general_exception)
