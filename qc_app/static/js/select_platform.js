function radio_column(chip) {
    col = document.createElement("td");
    radio = document.createElement("input");
    radio.setAttribute("type", "radio");
    radio.setAttribute("name", "genechipid");
    radio.setAttribute("value", chip["GeneChipId"]);
    radio.setAttribute("required", "required");
    col.appendChild(radio);
    return col;
}

function setup_genechips(genechip_data) {
    columns = ["GeneChipId", "GeneChipName"]
    submit_button = document.querySelector(
	"#select-platform-form input[type='submit']");
    elt = document.getElementById(
	"genechips-table").getElementsByTagName("tbody")[0];
    remove_children(elt);
    if((genechip_data === undefined) || genechip_data.length === 0) {
	row = document.createElement("tr");
	col = document.createElement("td");
	col.setAttribute("colspan", "3");
	text = document.createTextNode("No chips found for selected species");
	col.appendChild(text);
	row.appendChild(col);
	elt.appendChild(row);
	submit_button.setAttribute("disabled", true);
	return false;
    }

    submit_button.removeAttribute("disabled")
    genechip_data.forEach(chip => {
	row = document.createElement("tr");
	row.appendChild(radio_column(chip));
	columns.forEach(column => {
	    col = document.createElement("td");
	    content = document.createTextNode(chip[column]);
	    col.appendChild(content);
	    row.appendChild(col);
	});
	elt.appendChild(row);
    });
}

function genechips() {
    return JSON.parse(
	document.getElementById("select-platform-form").getAttribute(
	    "data-genechips"));
}

function update_genechips(event) {
    genec = genechips();

    species_elt = document.getElementById("species");

    if(event.target == species_elt) {
	setup_genechips(genec[species_elt.value.toLowerCase()]);
    }
}

function select_row_radio(row) {
    radio = row.getElementsByTagName(
	"td")[0].getElementsByTagName(
	    "input")[0];
    if(radio === undefined) {
	return false;
    }
    radio.setAttribute("checked", "checked");
    return true;
}
