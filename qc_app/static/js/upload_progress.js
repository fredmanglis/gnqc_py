function make_processing_indicator(elt) {
    var count = 0;
    return function() {
	var message = "Checking for errors: "
	if(count > 5) {
	    count = 1;
	}
	for(i = 0; i < count; i++) {
	    message = message + ".";
	}
	elt.innerHTML = message
	count = count + 1
    };
}

function make_progress_updater(file, indicator_elt) {
    var progress_bar = indicator_elt.querySelector("#progress-bar");
    var progress_text = indicator_elt.querySelector("#progress-text");
    var extra_text = indicator_elt.querySelector("#progress-extra-text");
    return function(event) {
	if(event.loaded <= file.size) {
	    var percent = Math.round((event.loaded / file.size) * 100);
	    progress_bar.value = percent
	    progress_text.innerHTML = "Uploading: " + percent + "%";
	    extra_text.setAttribute("class", "hidden")
	}

	if(event.loaded == event.total) {
	    progress_bar.value = 100;
	    progress_text.innerHTML = "Uploaded: 100%";
	    extra_text.setAttribute("class", null);
	    intv = setInterval(make_processing_indicator(extra_text), 400);
	    setTimeout(function() {clearTimeout(intv);}, 20000);
	}
    };
}

function setup_cancel_upload(request, indicator_elt) {
    document.getElementById("btn-cancel-upload").addEventListener(
	"click", function(event) {
	    event.preventDefault();
	    request.abort();
	    indicator_elt.setAttribute("class", "hidden");
	});
}

function setup_request(file, progress_indicator_elt) {
    var request = new XMLHttpRequest();
    var updater = make_progress_updater(file, progress_indicator_elt);
    request.upload.addEventListener("progress", updater);
    request.onload = function(event) {
	document.location.assign(request.responseURL);
    };
    setup_cancel_upload(request, progress_indicator_elt)
    return request;
}

function selected_filetype(radios) {
    for(idx = 0; idx < radios.length; idx++) {
	if(radios[idx].checked) {
	    return radios[idx].value;
	}
    }
}

function setup_formdata(form) {
    var formdata = new FormData();
    formdata.append(
	"speciesid",
	form.querySelector("#select_species01").value)
    formdata.append(
	"qc_text_file",
	form.querySelector("input[type='file']").files[0]);
    formdata.append(
	"filetype",
	selected_filetype(
	    Array.from(form.querySelectorAll("input[type='radio']"))));
    return formdata;
}

function upload_data(event) {
    event.preventDefault();

    var pindicator = document.getElementById("progress-indicator");

    var form = document.getElementsByTagName("form")[0];
    var the_file = form.querySelector("input[type='file']").files[0];
    if(the_file === undefined) {
	form.querySelector("#file_upload").parentElement.setAttribute(
	    "class", "invalid-input");
	form.querySelector("#no-file-error").setAttribute(
	    "style", "display: block;");
	return false;
    }
    pindicator.setAttribute("class", "modal");
    var formdata = setup_formdata(form);

    document.getElementById("progress-filename").innerHTML = the_file.name;
    var request = setup_request(
	the_file, document.getElementById("progress-indicator"));
    request.open(form.getAttribute("method"), form.getAttribute("action"));
    request.send(formdata);
    return false;
}


function setup_upload_handlers() {
    console.info("Setting up the upload handlers.")
    upload_form = document.getElementsByTagName("form")[0];
    upload_form.addEventListener("submit", upload_data);
}
