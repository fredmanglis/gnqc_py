/*
 * Read the file content and set the `data-preview-content` attribute on the
 * file element
 */
function read_first_n_lines(event,
			    fileelement,
			    numlines,
			    firstlineheading = true) {
    var thefile = fileelement.files[0];
    var reader = new FileReader();
    reader.addEventListener("load", (event) => {
	var filecontent = event.target.result.split(
	    "\n").slice(
		0, (numlines + (firstlineheading ? 1 : 0))).map(
		    (line) => {return line.trim("\r");});
	fileelement.setAttribute(
	    "data-preview-content", JSON.stringify(filecontent));
	display_preview(event);
    })
    reader.readAsText(thefile);
}

function remove_rows(preview_table) {
    var table_body = preview_table.getElementsByTagName("tbody")[0];
    while(table_body.children.length > 0) {
	table_body.removeChild(table_body.children.item(0));
    }
}

/*
 * Display error row
 */
function display_error_row(preview_table, error_message) {
    remove_rows(preview_table);
    row = document.createElement("tr");
    cell = document.createElement("td");
    cell.setAttribute("colspan", 4);
    cell.innerHTML = error_message;
    row.appendChild(cell);
    preview_table.getElementsByTagName("tbody")[0].appendChild(row);
}

function strip(str, chars) {
    var end = str.length;
    var start = 0
    for(var j = str.length; j > 0; j--) {
	if(!chars.includes(str[j - 1])) {
	    break;
	}
	end = end - 1;
    }
    for(var i = 0; i < end; i++) {
	if(!chars.includes(str[i])) {
	    break;
	}
	start = start + 1;
    }
    return str.slice(start, end);
}

function process_preview_data(preview_data, separator, delimiter) {
    return preview_data.map((line) => {
	return line.split(separator).map((field) => {
	    return strip(field, delimiter);
	});
    });
}

function render_preview(preview_table, preview_data) {
    remove_rows(preview_table);
    var table_body = preview_table.getElementsByTagName("tbody")[0];
    preview_data.forEach((line) => {
	var row = document.createElement("tr");
	line.forEach((field) => {
	    var cell = document.createElement("td");
	    cell.innerHTML = field;
	    row.appendChild(cell);
	});
	table_body.appendChild(row);
    });
}

/*
 * Display a preview of the data, relying on the user's selection.
 */
function display_preview(event) {
    var data_preview_table = document.getElementById("tbl:samples-preview");
    remove_rows(data_preview_table);

    var separator = document.getElementById("select:separator").value;
    if(separator === "other") {
	separator = document.getElementById("txt:separator").value;
    }
    if(separator == "") {
	display_error_row(data_preview_table, "Please provide a separator.");
	return false;
    }

    var delimiter = document.getElementById("txt:delimiter").value;

    var firstlineheading = document.getElementById("chk:heading").checked;

    var fileelement = document.getElementById("file:samples");
    var preview_data = JSON.parse(
	fileelement.getAttribute("data-preview-content") || "[]");
    if(preview_data.length == 0) {
	display_error_row(
	    data_preview_table,
	    "No file data to preview. Check that file is provided.");
    }

    render_preview(data_preview_table, process_preview_data(
	preview_data.slice(0 + (firstlineheading ? 1 : 0)),
	separator,
	delimiter));
}

document.getElementById("chk:heading").addEventListener(
    "change", display_preview);
document.getElementById("select:separator").addEventListener(
    "change", display_preview);
document.getElementById("txt:separator").addEventListener(
    "keyup", display_preview);
document.getElementById("txt:delimiter").addEventListener(
    "keyup", display_preview);
document.getElementById("file:samples").addEventListener(
    "change", (event) => {
	read_first_n_lines(event,
			   document.getElementById("file:samples"),
			   30,
			   document.getElementById("chk:heading").checked);
    });
