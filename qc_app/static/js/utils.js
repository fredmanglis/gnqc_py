function remove_children(element) {
    Array.from(element.children).forEach(child => {
	element.removeChild(child);
    });
}

function trigger_change_event(element) {
    evt = new Event("change");
    element.dispatchEvent(evt);
}
