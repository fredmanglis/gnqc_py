"""Package handling upload of files."""
from flask import Blueprint

from .rqtl2 import rqtl2

upload = Blueprint("upload", __name__)
upload.register_blueprint(rqtl2, url_prefix="/rqtl2")
