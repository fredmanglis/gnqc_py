"""The Quality-Control Web Application entry point"""

import os

from flask import Flask

from .entry import entrybp
from .upload import upload
from .parse import parsebp
from .samples import samples
from .dbinsert import dbinsertbp
from .errors import register_error_handlers

def instance_path():
    """Retrieve the `instance_path`. Raise an exception if not defined."""
    path = os.getenv("QCAPP_INSTANCE_PATH", None)
    if path is None:
        raise Exception((
            "Configuration Error: Set the `QCAPP_INSTANCE_PATH` environment "
            "variable."))

    return path

def create_app(instance_dir):
    """The application factory"""
    app = Flask(
        __name__, instance_path=instance_dir, instance_relative_config=True)
    app.config.from_pyfile(os.path.join(os.getcwd(), "etc/default_config.py"))
    app.config.from_pyfile("config.py") # Override defaults with instance path

    # setup blueprints
    app.register_blueprint(entrybp, url_prefix="/")
    app.register_blueprint(parsebp, url_prefix="/parse")
    app.register_blueprint(upload, url_prefix="/upload")
    app.register_blueprint(dbinsertbp, url_prefix="/dbinsert")
    app.register_blueprint(samples, url_prefix="/samples")

    register_error_handlers(app)
    return app
