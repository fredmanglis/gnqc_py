"""Functions for accessing the database relating to datasets."""
from datetime import date
from typing import Optional

import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

def geno_datasets_by_species_and_population(
        conn: mdb.Connection,
        speciesid: int,
        populationid: int) -> tuple[dict, ...]:
    """Retrieve all genotypes datasets by species and population"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute(
            "SELECT gf.* FROM InbredSet AS iset INNER JOIN GenoFreeze AS gf "
            "ON iset.InbredSetId=gf.InbredSetId "
            "WHERE iset.SpeciesId=%(sid)s AND iset.InbredSetId=%(pid)s",
            {"sid": speciesid, "pid": populationid})
        return tuple(dict(row) for row in cursor.fetchall())

def geno_dataset_by_id(conn: mdb.Connection, dataset_id: int) -> dict:
    """Retrieve genotype dataset by ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM GenoFreeze WHERE Id=%s",
                       (dataset_id,))
        return dict(cursor.fetchone())

def probeset_studies_by_species_and_population(
        conn: mdb.Connection,
        speciesid: int,
        populationid: int) -> tuple[dict, ...]:
    """Retrieve all probesets"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute(
            "SELECT pf.* FROM InbredSet AS iset INNER JOIN ProbeFreeze AS pf "
            "ON iset.InbredSetId=pf.InbredSetId "
            "WHERE iset.SpeciesId=%(sid)s AND iset.InbredSetId=%(pid)s",
            {"sid": speciesid, "pid": populationid})
        return tuple(dict(row) for row in cursor.fetchall())

def probeset_datasets_by_study(conn: mdb.Connection,
                               studyid: int) -> tuple[dict, ...]:
    """Retrieve all probeset databases by study."""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM ProbeSetFreeze WHERE ProbeFreezeId=%s",
                       (studyid,))
        return tuple(dict(row) for row in cursor.fetchall())

def probeset_study_by_id(conn: mdb.Connection, studyid: int) -> dict:
    """Retrieve ProbeSet study by ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM ProbeFreeze WHERE Id=%s",
                       (studyid,))
        return dict(cursor.fetchone())

def probeset_create_study(conn: mdb.Connection,#pylint: disable=[too-many-arguments]
                          populationid: int,
                          platformid: int,
                          tissueid: int,
                          studyname: str,
                          studyfullname: str = "",
                          studyshortname: str = ""):
    """Create a new ProbeSet study."""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        studydata = {
            "platid": platformid,
            "tissueid": tissueid,
            "name": studyname,
            "fname": studyfullname or studyname,
            "sname": studyshortname,
            "today": date.today().isoformat(),
            "popid": populationid
        }
        cursor.execute(
            """
            INSERT INTO ProbeFreeze(
              ChipId, TissueId, Name, FullName, ShortName, CreateTime,
              InbredSetId
            ) VALUES (
              %(platid)s, %(tissueid)s, %(name)s, %(fname)s, %(sname)s,
              %(today)s, %(popid)s
            )
            """,
            studydata)
        studyid = cursor.lastrowid
        cursor.execute("UPDATE ProbeFreeze SET ProbeFreezeId=%s",
                       (studyid,))
        return {**studydata, "studyid": studyid}

def probeset_create_dataset(conn: mdb.Connection,#pylint: disable=[too-many-arguments]
                            studyid: int,
                            averageid: int,
                            datasetname: str,
                            datasetfullname: str,
                            datasetshortname: str="",
                            public: bool = True,
                            datascale="log2") -> dict:
    """Create a new ProbeSet dataset."""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        dataset = {
            "studyid": studyid,
            "averageid": averageid,
            "name2": datasetname,
            "fname": datasetfullname,
            "name": datasetshortname,
            "sname": datasetshortname,
            "today": date.today().isoformat(),
            "public": 2 if public else 0,
            "datascale": datascale
        }
        cursor.execute(
            """
            INSERT INTO ProbeSetFreeze(
              ProbeFreezeId, AvgId, Name, Name2, FullName, ShortName,
              CreateTime, public, DataScale)
            VALUES(
              %(studyid)s, %(averageid)s, %(name)s, %(name2)s, %(fname)s,
              %(sname)s, %(today)s, %(public)s, %(datascale)s)
            """,
            dataset)
        return {**dataset, "datasetid": cursor.lastrowid}

def probeset_dataset_by_id(
        conn: mdb.Connection, datasetid: int) -> Optional[dict]:
    """Fetch a ProbeSet dataset by its ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM ProbeSetFreeze WHERE Id=%s",
                       (datasetid,))
        result = cursor.fetchone()
        if bool(result):
            return dict(result)

    return None
