"""Handle db interactions for tissue."""
from typing import Optional

import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

def all_tissues(conn: mdb.Connection) -> tuple[dict, ...]:
    """All available tissue."""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM Tissue ORDER BY TissueName")
        return tuple(dict(row) for row in cursor.fetchall())

def tissue_by_id(conn: mdb.Connection, tissueid: int) -> Optional[dict]:
    """Retrieve a tissue by its ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM Tissue WHERE Id=%s",
                       (tissueid,))
        result = cursor.fetchone()
        if bool(result):
            return dict(result)

    return None
