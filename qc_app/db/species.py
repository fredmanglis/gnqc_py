"""Database functions for species."""
import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

def species(conn: mdb.Connection) -> tuple:
    "Retrieve the species from the database."
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute(
            "SELECT SpeciesId, SpeciesName, LOWER(Name) AS Name, MenuName, "
            "FullName FROM Species")
        return tuple(cursor.fetchall())

    return tuple()

def species_by_id(conn: mdb.Connection, speciesid) -> dict:
    "Retrieve the species from the database by id."
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute(
            "SELECT SpeciesId, SpeciesName, LOWER(Name) AS Name, MenuName, "
            "FullName FROM Species WHERE SpeciesId=%s",
            (speciesid,))
        return cursor.fetchone()
