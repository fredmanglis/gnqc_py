"""Handle db interactions for platforms."""
from typing import Optional

import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

def platforms_by_species(
        conn: mdb.Connection, speciesid: int) -> tuple[dict, ...]:
    """Retrieve platforms by the species"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM GeneChip WHERE SpeciesId=%s "
                       "ORDER BY GeneChipName ASC",
                       (speciesid,))
        return tuple(dict(row) for row in cursor.fetchall())

def platform_by_id(conn: mdb.Connection, platformid: int) -> Optional[dict]:
    """Retrieve a platform by its ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM GeneChip WHERE Id=%s",
                       (platformid,))
        result = cursor.fetchone()
        if bool(result):
            return dict(result)

    return None
