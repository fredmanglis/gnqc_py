"""Functions for db interactions for averaging methods"""
from typing import Optional

import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

def averaging_methods(conn: mdb.Connection) -> tuple[dict, ...]:
    """Fetch all available averaging methods"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM AvgMethod")
        return tuple(dict(row) for row in cursor.fetchall())

def averaging_method_by_id(
        conn: mdb.Connection, averageid: int) -> Optional[dict]:
    """Fetch the averaging method by its ID"""
    with conn.cursor(cursorclass=DictCursor) as cursor:
        cursor.execute("SELECT * FROM AvgMethod WHERE Id=%s",
                       (averageid,))
        result = cursor.fetchone()
        if bool(result):
            return dict(result)

    return None
