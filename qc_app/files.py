"""Utilities to deal with uploaded files."""
import hashlib
from pathlib import Path
from typing import Union
from datetime import datetime
from flask import current_app

from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage

def save_file(fileobj: FileStorage, upload_dir: Path) -> Union[Path, bool]:
    """Save the uploaded file and return the path."""
    if not bool(fileobj):
        return False
    hashed_name = hashlib.sha512(
        f"{fileobj.filename}::{datetime.now().isoformat()}".encode("utf8")
    ).hexdigest()
    filename = Path(secure_filename(hashed_name)) # type: ignore[arg-type]
    if not upload_dir.exists():
        upload_dir.mkdir()

    filepath = Path(upload_dir, filename)
    fileobj.save(filepath)
    return filepath

def fullpath(filename: str):
    """Get a file's full path. This makes use of `flask.current_app`."""
    return Path(current_app.config["UPLOAD_FOLDER"], filename).absolute()
