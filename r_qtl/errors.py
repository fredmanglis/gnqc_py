"""R/qtl and R/qtl2 error types."""

class RQTLError(Exception):
    """Base class for R/qtl and R/qtl2 errors."""

class InvalidFormat(RQTLError):
    """Raised when the format of the file(s) is invalid."""

class MissingFileError(InvalidFormat):
    """
    Raise when at least one file listed in the control file is missing from the
    R/qtl2 bundle.
    """
