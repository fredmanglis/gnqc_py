"""Build common script-entry structure."""
from logging import Logger
from typing import Callable
from argparse import Namespace

from redis import Redis
from MySQLdb import Connection

from qc_app import jobs
from qc_app.db_utils import database_connection
from qc_app.check_connections import check_db, check_redis

from scripts.redis_logger import setup_redis_logger

def build_main(args: Namespace,
               run_fn: Callable[[Connection, Namespace], int],
               logger: Logger,
               loglevel: str = "INFO") -> Callable[[],int]:
    """Build a function to be used as an entry-point for scripts."""
    def main():
        check_db(args.databaseuri)
        check_redis(args.redisuri)
        if not args.rqtl2bundle.exists():
            logger.error("File not found: '%s'.", args.rqtl2bundle)
            return 2

        with (Redis.from_url(args.redisuri, decode_responses=True) as rconn,
              database_connection(args.databaseuri) as dbconn):
            fqjobid = jobs.job_key(jobs.jobsnamespace(), args.jobid)
            logger.addHandler(setup_redis_logger(
                rconn,
                fqjobid,
                f"{fqjobid}:log-messages",
                args.redisexpiry))
            logger.setLevel(loglevel)
            return run_fn(dbconn, args)

    return main
