"""Load genotypes from R/qtl2 bundle into the database."""
import sys
import traceback
from pathlib import Path
from zipfile import ZipFile
from functools import reduce
from typing import Iterator, Optional
from logging import Logger, getLogger, StreamHandler

import MySQLdb as mdb
from MySQLdb.cursors import DictCursor

from r_qtl import r_qtl2 as rqtl2
from r_qtl import r_qtl2_qc as rqc

from functional_tools import take

from scripts.rqtl2.entry import build_main
from scripts.rqtl2.cli_parser import add_common_arguments
from scripts.cli_parser import init_cli_parser, add_global_data_arguments

def insert_markers(dbconn: mdb.Connection,
                   speciesid: int,
                   markers: tuple[str, ...],
                   pmapdata: Optional[Iterator[dict]]) -> int:
    """Insert genotype and genotype values into the database."""
    mdata = reduce(#type: ignore[var-annotated]
        lambda acc, row: ({#type: ignore[arg-type, return-value]
            **acc, row["id"]: {
                key: val
                for key,val in row.items()
                if key != "id"
            }
        }),
        (pmapdata or tuple()),
        {})
    with dbconn.cursor() as cursor:
        cursor.executemany(
            "INSERT INTO Geno(SpeciesId, Name, Marker_Name, Chr, Mb) "
            "VALUES (%(speciesid)s, %(marker)s, %(marker)s, %(chr)s, %(pos)s) "
            "ON DUPLICATE KEY UPDATE SpeciesId=SpeciesId",
            tuple({
                "speciesid": speciesid,
                "marker": marker,
                "chr": mdata.get(marker, {}).get("chr"),
                "pos": mdata.get(marker, {}).get("pos")
            } for marker in markers))
        return cursor.rowcount

def insert_individuals(dbconn: mdb.Connection,
                       speciesid: int,
                       individuals: tuple[str, ...]) -> int:
    """Insert individuals/samples into the database."""
    with dbconn.cursor() as cursor:
        cursor.executemany(
            "INSERT INTO Strain(Name, Name2, SpeciesId) "
            "VALUES(%(id)s, %(id)s, %(speciesid)s) "
            "ON DUPLICATE KEY UPDATE Name=Name",
            tuple({"speciesid": speciesid, "id": individual}
                  for individual in individuals))
        return cursor.rowcount

def cross_reference_individuals(dbconn: mdb.Connection,
                                speciesid: int,
                                populationid: int,
                                individuals: tuple[str, ...]) -> int:
    """Cross reference any inserted individuals."""
    with dbconn.cursor(cursorclass=DictCursor) as cursor:
        paramstr = ", ".join(["%s"] * len(individuals))
        cursor.execute("SELECT Id FROM Strain "
                       f"WHERE SpeciesId=%s AND Name IN ({paramstr})",
                       (speciesid,) + individuals)
        ids = ({"popid": populationid, "indid": row["Id"]}
               for row in cursor.fetchall())
        cursor.executemany(
            "INSERT INTO StrainXRef(InbredSetId, StrainId) "
            "VALUES(%(popid)s, %(indid)s) "
            "ON DUPLICATE KEY UPDATE InbredSetId=InbredSetId",
            tuple(ids))
        return cursor.rowcount

def insert_genotype_data(dbconn: mdb.Connection,
                         speciesid: int,
                         genotypes: tuple[dict, ...],
                         individuals: tuple[str, ...]) -> tuple[
                             int, tuple[dict, ...]]:
    """Insert the genotype data values into the database."""
    with dbconn.cursor(cursorclass=DictCursor) as cursor:
        paramstr = ", ".join(["%s"] * len(individuals))
        cursor.execute(
            f"SELECT Id, Name FROM Strain WHERE Name IN ({paramstr})",
            individuals)
        indids = {row["Name"]: row["Id"] for row in cursor.fetchall()}
        markers = tuple(key for key in genotypes[0].keys() if key != "id")
        paramstr = ", ".join(["%s"] * len(markers))
        cursor.execute(
            f"SELECT Id, Name FROM Geno WHERE SpeciesId=%s AND Name IN ({paramstr})",
            (speciesid,)+markers)
        markerids = {row["Name"]: row["Id"] for row in cursor.fetchall()}
        cursor.execute("SELECT MAX(Id) AS lastid FROM GenoData")
        lastid = cursor.fetchone()["lastid"]
        data = tuple(
            {**row, "gid": gid}
            for gid, row in enumerate(
                    ({
                        "value": innerrow[marker],
                        "indid": indids[innerrow["id"]],
                        "markerid": markerids[marker]
                    } for innerrow in genotypes for marker in
                     (key for key in innerrow.keys() if key != "id")),
                    start=lastid+1))
        cursor.executemany(
            "INSERT INTO GenoData(Id, StrainId, value) "
            "VALUES(%(gid)s, %(indid)s, %(value)s)",
            data)
        return cursor.rowcount, tuple({
            "dataid": row["gid"],
            "markerid": row["markerid"]
        } for row in data)

def cross_reference_genotypes(dbconn: mdb.Connection,
                              speciesid: int,
                              datasetid: int,
                              dataids: tuple[dict, ...],
                              gmapdata: Optional[Iterator[dict]]) -> int:
    """Cross-reference the data to the relevant dataset."""
    _rows, markers, mdata = reduce(#type: ignore[var-annotated]
        lambda acc, row: (#type: ignore[return-value,arg-type]
            (acc[0] + (row,)), (acc[1] + (row["id"],)),
            {
                **acc[2], row["id"]: {
                    key: val
                    for key,val in row.items()
                    if key != "id"
                }
            }),
        (gmapdata or tuple()),
        (tuple(), tuple(), {}))

    with dbconn.cursor(cursorclass=DictCursor) as cursor:
        paramstr = ", ".join(["%s"] * len(markers))
        cursor.execute("SELECT Id, Name FROM Geno "
                       f"WHERE SpeciesId=%s AND Name IN ({paramstr})",
                       (speciesid,) + markers)
        markersdict = {row["Id"]: row["Name"] for row in cursor.fetchall()}
        cursor.executemany(
            "INSERT INTO GenoXRef(GenoFreezeId, GenoId, DataId, cM) "
            "VALUES(%(datasetid)s, %(markerid)s, %(dataid)s, %(pos)s) "
            "ON DUPLICATE KEY UPDATE GenoFreezeId=GenoFreezeId",
            tuple({
                **row,
                "datasetid": datasetid,
                "pos": mdata.get(markersdict.get(
                    row.get("markerid"), {}), {}).get("pos")
            } for row in dataids))
        return cursor.rowcount

def install_genotypes(#pylint: disable=[too-many-arguments, too-many-locals]
        dbconn: mdb.Connection,
                      speciesid: int,
                      populationid: int,
                      datasetid: int,
                      rqtl2bundle: Path,
                      logger: Logger = getLogger()) -> int:
    """Load any existing genotypes into the database."""
    count = 0
    with ZipFile(str(rqtl2bundle.absolute()), "r") as zfile:
        try:
            logger.info("Validating bundle")
            rqc.validate_bundle(zfile)
            logger.info("Bundle validated successfully.")
            logger.info(("Loading genotypes. This could take a while. "
                         "Please be patient."))

            cdata = rqtl2.control_data(zfile)
            genotypes = rqtl2.genotype_data(zfile)
            while True:
                batch = tuple(take(genotypes, 5000))
                if len(batch) == 0:
                    logger.info("Loading Genotypes complete!")
                    logger.info(
                        "Total rows processed: %s", count)
                    break

                insert_markers(
                    dbconn,
                    speciesid,
                    tuple(key for key in batch[0].keys() if key != "id"),
                    (rqtl2.file_data(zfile, "pmap", cdata) if "pmap" in cdata
                     else None))
                individuals = tuple(row["id"] for row in batch)
                insert_individuals(dbconn, speciesid, individuals)
                cross_reference_individuals(
                    dbconn, speciesid, populationid, individuals)
                _num_rows, dataids = insert_genotype_data(
                    dbconn, speciesid, batch, individuals)
                cross_reference_genotypes(
                    dbconn,
                    speciesid,
                    datasetid,
                    dataids,
                    (rqtl2.file_data(zfile, "gmap", cdata)
                     if "gmap" in cdata else None))
                count = count + len(batch)
        except rqtl2.InvalidFormat as exc:
            logger.error(str(exc))
            logger.info("There are no genotypes to load.")
        except Exception as _exc:# pylint: disable=[broad-except]
            # Narrow this exception with time.
            logger.error("Failing with exception: %s", traceback.format_exc())
            return 3

    return 0

if __name__ == "__main__":

    def cli_args():
        """Process command-line arguments for install_genotypes"""
        parser = add_common_arguments(add_global_data_arguments(init_cli_parser(
            "install_genotypes",
            "Parse genotypes from R/qtl2 bundle into the database.")))

        return parser.parse_args()

    thelogger = getLogger("install_genotypes")
    thelogger.addHandler(StreamHandler(stream=sys.stderr))
    main = build_main(
        cli_args(),
        lambda dbconn, args: install_genotypes(dbconn,
                                               args.speciesid,
                                               args.populationid,
                                               args.datasetid,
                                               args.rqtl2bundle),
        thelogger,
        "INFO")
    sys.exit(main())
