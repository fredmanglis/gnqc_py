"""Utilities for CLI parsers specific to R/qtl2 parsing scripts."""
from pathlib import Path
from argparse import ArgumentParser

def add_common_arguments(parser: ArgumentParser) -> ArgumentParser:
    """Add common arguments to the CLI parser."""
    parser.add_argument("datasetid",
                        type=int,
                        help="The dataset to which the data belongs.")
    parser.add_argument("rqtl2bundle",
                        type=Path,
                        help="Path to R/qtl2 bundle zip file.")
    return parser
