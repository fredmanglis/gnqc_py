"""Common utilities for CLI parsers"""
from uuid import UUID
from typing import Optional
from argparse import ArgumentParser

def init_cli_parser(program: str, description: Optional[str] = None) -> ArgumentParser:
    """Initialise the CLI arguments parser."""
    parser = ArgumentParser(prog=program, description=description)

    parser.add_argument(
        "databaseuri", type=str, help="URI to connect to MariaDB")
    parser.add_argument(
        "redisuri", type=str, help="URI to connect to the redis server.")
    parser.add_argument(
        "redisprefix", type=str, help="A prefix to the job ID.")
    parser.add_argument("jobid", type=UUID, help="Job ID that this belongs to")
    parser.add_argument(
        "--redisexpiry",
        type=int,
        default=86400,
        help="How long to keep any redis keys around.")
    return parser

def add_global_data_arguments(parser: ArgumentParser) -> ArgumentParser:
    """Add the global (present in nearly ALL scripts) CLI arguments."""
    parser.add_argument("speciesid",
                        type=int,
                        help="Species to which bundle relates.")
    parser.add_argument("populationid",
                        type=int,
                        help="Population to group data under")
    return parser
