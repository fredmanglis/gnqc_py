"""Run the application"""

from qc_app import create_app, instance_path
from qc_app.check_connections import check_db, check_redis

def check_and_build_app():
    """Setup the application for running."""
    # Setup the app
    appl = create_app(instance_path())

    # Check connections
    check_db(appl.config["SQL_URI"])
    check_redis(appl.config["REDIS_URL"])
    return appl

app = check_and_build_app()

if __name__ == "__main__":
    # Run the app
    app.run()
