(define-module (gnqc-py)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)

  #:use-module ((gnu packages check) #:select (python-pylint))
  #:use-module ((gnu packages python-check) #:select (python-mypy))

  #:use-module ((gn packages genenetwork) #:select (gnqc-py) #:prefix gn:))

(define %source-dir (dirname (dirname (current-source-directory))))

(define vcs-file?
  (or (git-predicate %source-dir)
      (const #t)))

(define-public gnqc-py
  (package
    (inherit gn:gnqc-py)
    (source
     (local-file "../.."
		 "gnqc-py-checkout"
		 #:recursive? #t
		 #:select? vcs-file?))))

(define-public gnqc-py-all-tests
  (package
    (inherit gnqc-py)
    (arguments
     (substitute-keyword-arguments (package-arguments gnqc-py)
       ((#:phases #~%standard-phases)
	#~(modify-phases #$phases
	    (add-before 'build 'pylint
	      (lambda _
		(invoke "pylint" "setup.py" "wsgi.py" "tests" "quality_control"
			"qc_app" "r_qtl" "scripts")))
	    (add-after 'pylint 'mypy
	      (lambda _
		(invoke "mypy" ".")))))))
    (native-inputs
     (modify-inputs (package-native-inputs gnqc-py)
		    (prepend python-mypy)
		    (prepend python-pylint)))))

gnqc-py
