"""Contain logic for checking average files"""
from typing import Union

from .utils import cell_error
from .errors import InvalidValue

def invalid_value(line_number: int, column_number: int, val: str) -> Union[
        InvalidValue, None]:
    """Return an `InvalidValue` object if `val` is not a valid "averages"
    value."""
    return cell_error(
        r"^([0-9]+\.[0-9]{3}|[0-9]+\.?0*)$", val, line=line_number,
        column=column_number, value=val, message=(
            f"Invalid value '{val}'. "
            "Expected string representing a number with exactly three "
            "decimal places."))
