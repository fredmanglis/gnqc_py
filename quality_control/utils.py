"""Utilities that might be useful elsewhere."""
import re
from collections import namedtuple

from .errors import InvalidValue

ProgressIndicator = namedtuple(
    "ProgressIndicator", ("filesize", "processedsize", "currentline", "percent"))

def make_progress_calculator(filesize: int):
    """
    Returns a function that takes two arguments, `linenumber` and `linetext` and
    return a `ProgressIndicator` object with the computed progress.
    """
    processedsize = 0
    def __calculator__(linenumber: int, linetext: str) -> ProgressIndicator:
        nonlocal processedsize
        processedsize = processedsize + len(linetext)
        return ProgressIndicator(
            filesize, processedsize, linenumber,
            ((processedsize/filesize) * 100))

    return __calculator__

def cell_error(pattern, val, **error_kwargs):
    "Return the error in the cell"
    if re.search(pattern, val):
        return None
    if re.search(r"^0\.0+$", val) or re.search("^0+$", val):
        return None
    return InvalidValue(**error_kwargs)
