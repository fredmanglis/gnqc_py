"""Contain logic for checking standard error files"""
from typing import Union

from .utils import cell_error
from .errors import InvalidValue

def invalid_value(
        line_number: int, column_number: int, val: str) -> Union[
            InvalidValue, None]:
    """
    Returns a `quality_control.errors.InvalidValue` object in the case where
    `val` is not a valid input for standard error files, otherwise, it returns
    `None`.
    """
    return cell_error(
        r"^([0-9]+\.[0-9]{6,}|[0-9]+\.?0*)$", val, line=line_number,
        column=column_number, value=val, message=(
            f"Invalid value '{val}'. "
            "Expected string representing a number with at least six "
            "decimal places."))
