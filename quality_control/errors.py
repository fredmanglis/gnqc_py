"""Hold exceptions for QC package"""

from collections import namedtuple

InvalidValue = namedtuple(
    "InvalidValue", ("line", "column", "value", "message"))

DuplicateHeading = namedtuple(
    "DuplicateHeading", ("line", "columns", "heading", "message"))

InconsistentColumns = namedtuple(
    "InconsistentColumns", ("line", "header_count", "contents_count", "message"))
