"Common file utilities"
from typing import Union
from pathlib import Path
from zipfile import ZipFile, is_zipfile

def open_file(filepath: Union[str, Path]):
    "Transparently open both TSV and ZIP files"
    if not is_zipfile(filepath):
        return open(filepath, encoding="utf-8")

    with ZipFile(filepath, "r") as zfile:
        return zfile.open(zfile.infolist()[0], "r")
